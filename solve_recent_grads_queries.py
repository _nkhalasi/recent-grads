import csv
from typing import List, Callable, Tuple, Dict, Any
from itertools import groupby

# rank,major_code,major,total,men,women,major_category,sharewomen,sample_size,employed,full_time,part_time,full_time_year_round,unemployed,unemployment_rate,median,p25th,p75th,college_jobs,non_college_jobs,low_wage_jobs
# * Find the "Major" taken by
#     * most men
#     * most women
# * Find the "Major Category" taken by most men
# * Find the "Major Category" taken by least women
# * Find the "Major" that gets most jobs
#     > Hint: Consider of all types


class RecentGradData():
    major: str
    major_code: str
    major_category: str
    total_students: int
    men_students: int
    women_students: int
    full_time_emp_cnt: int
    part_time_emp_cnt: int
    full_time_year_round_emp_cnt: int

    def __init__(self, major_code, major, major_category, total_students, men_students,
                 women_students, full_time_emp_cnt, part_time_emp_cnt, full_time_year_round_emp_cnt):
        self.major_code = major_code
        self.major = major
        self.major_category = major_category
        self.total_students = int(total_students) if total_students else 0
        self.men_students = int(men_students) if men_students else 0
        self.women_students = int(women_students) if women_students else 0
        self.full_time_emp_cnt = \
            int(full_time_emp_cnt) if full_time_emp_cnt else 0
        self.part_time_emp_cnt = \
            int(part_time_emp_cnt) if part_time_emp_cnt else 0
        self.full_time_year_round_emp_cnt = \
            int(full_time_year_round_emp_cnt) if full_time_year_round_emp_cnt else 0

    def __repr__(self):
        return """
RecentGradData(major_code={}, major={}, major_category={}, total_students={}, men_students={}, women_students={}, full_time_emp_count={}, part_time_emp_count={}, full_time_year_round_emp_count={})
""".strip().format(self.major_code, self.major, self.major_category, self.total_students, self.men_students, self.women_students, self.full_time_emp_cnt, self.part_time_emp_cnt, self.full_time_year_round_emp_cnt)


def load_data(data_file: str) -> List[RecentGradData]:
    grads_data = list()
    with open(data_file) as df:
        df_reader = csv.reader(df)
        next(df_reader, None)  # skip the header row
        grads_data.extend(
            [RecentGradData(r[1], r[2], r[6], r[3], r[4], r[5],
                            r[10], r[11], r[12]) for r in df_reader]
        )
    return grads_data


def major_category_key_fn(rcd: RecentGradData) -> str:
    return rcd.major_category


def men_students_key_fn(rcd: RecentGradData) -> int:
    return rcd.men_students


def women_students_key_fn(rcd: RecentGradData) -> int:
    return rcd.women_students


def total_jobs_key_fn(rcd: RecentGradData) -> int:
    return rcd.full_time_emp_cnt + rcd.part_time_emp_cnt + rcd.full_time_year_round_emp_cnt


def _sort_data_generic(data: List[RecentGradData], sortFn: Callable[[RecentGradData], Any]) -> List[RecentGradData]:
    sorted_data = sorted(data, key=sortFn, reverse=True)
    return sorted_data


def _sort_data1(data: List[RecentGradData], sortFn: Callable[[RecentGradData], int]) -> List[RecentGradData]:
    return _sort_data_generic(data, sortFn)


def _sort_data2(data: List[RecentGradData], sortFn: Callable[[RecentGradData], str]) -> List[RecentGradData]:
    return _sort_data_generic(data, sortFn)


def major_taken_by_most_men(data: List[RecentGradData]) -> Tuple[str, str, str, int]:
    top_row = _sort_data1(data, men_students_key_fn)[0]
    return top_row.major_code, top_row.major, top_row.major_category, top_row.men_students


def major_taken_by_most_women(data: List[RecentGradData]) -> Tuple[str, str, str, int]:
    top_row = _sort_data1(data, women_students_key_fn)[0]
    return top_row.major_code, top_row.major, top_row.major_category, top_row.women_students


def top10_major_giving_most_jobs(data: List[RecentGradData]) -> List[Tuple[str, str, str, int, int, int, int, int, int]]:
    def _subset_data(rcd: RecentGradData) -> Tuple[str, str, str, int, int, int, int, int, int]:
        return rcd.major_code, rcd.major, rcd.major_category, rcd.men_students, rcd.women_students, rcd.full_time_emp_cnt, rcd.part_time_emp_cnt, rcd.full_time_year_round_emp_cnt, rcd.full_time_emp_cnt + rcd.part_time_emp_cnt + rcd.full_time_year_round_emp_cnt

    return list(map(
        lambda rcd: _subset_data(rcd),
        _sort_data1(data, total_jobs_key_fn)[:10]
    ))


def _major_category_taken_by(data: List[RecentGradData],
                             groupbyFn: Callable[[RecentGradData], str],
                             keyFn: Callable[[RecentGradData], int],
                             most_or_least: bool):  # most=True, least=False
    sorted_data = _sort_data2(data, groupbyFn)
    grouped_data = groupby(sorted_data, key=groupbyFn)
    return sorted(
        [(k, sum(list(map(keyFn, list(v))))) for k, v in grouped_data],
        key=lambda x: x[1], reverse=most_or_least
    )[0]


def major_category_taken_by_most_men(data: List[RecentGradData]) -> Tuple[str, int]:
    return _major_category_taken_by(data, major_category_key_fn, men_students_key_fn, True)


def major_category_taken_by_least_women(data: List[RecentGradData]) -> Tuple[str, int]:
    return _major_category_taken_by(data, major_category_key_fn, women_students_key_fn, False)


if __name__ == "__main__":
    log_separator_line = "--**--" * 15
    data = load_data("0013-recent-grads.csv")

    print(major_taken_by_most_men(data))
    print(log_separator_line)
    print(major_taken_by_most_women(data))
    print(log_separator_line)
    for d in top10_major_giving_most_jobs(data):
        print(d)
    print(log_separator_line)
    print(major_category_taken_by_most_men(data))
    print(log_separator_line)
    print(major_category_taken_by_least_women(data))
    print(log_separator_line)
